package cn.ck;

import ai.onnxruntime.OnnxTensor;
import ai.onnxruntime.OrtEnvironment;
import ai.onnxruntime.OrtException;
import ai.onnxruntime.OrtSession;
import cn.ck.config.ODConfig;
import cn.ck.utils.Letterbox;
import org.opencv.core.*;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.nio.FloatBuffer;
import java.util.*;

import static cn.ck.PlateDetection.getImagePathMap;

/**
 * @Author IOError
 * @Date 2025/1/20 16:25
 * @Version 1.0
 */
public class PaddleYOLOv3MobileNet {

    private static final String MODEL_PATH = "src/main/resources/model/yolov3_mobilenet_v1_roadsign.onnx";
    private static final String[] LABELS = new String[]{"speedLimit","crosswalk","trafficLight","stop"};
    private static final String TEST_DATASET_PATH = "images/road";

    public static void main(String[] args) throws OrtException {
        float threshold = 0.25F;

        OrtSession.SessionOptions sessionOptions = new OrtSession.SessionOptions();
        // 使用gpu,需要本机按钻过cuda，并修改pom.xml，不安装也能运行本程序
        // sessionOptions.addCUDA(0);

        try(OrtEnvironment environment = OrtEnvironment.getEnvironment();
            OrtSession session = environment.createSession(MODEL_PATH, sessionOptions)){

            Map<String, String> map = getImagePathMap(TEST_DATASET_PATH);
            for (String fileName : map.keySet()) {
                Mat img = Imgcodecs.imread(map.get(fileName));

                //缩放比例
                float[] scale = new float[]{1F, 1F};
                //高宽
                float[] shape = new float[]{img.height(), img.width()};

                HashMap<String, OnnxTensor> onnxTensorHashMap = new HashMap<>();
                onnxTensorHashMap.put("image", getRGBImage(environment, img, new Letterbox(608, 608)));
                onnxTensorHashMap.put("im_shape", OnnxTensor.createTensor(environment, FloatBuffer.wrap(shape), new long[]{1, 2}));
                onnxTensorHashMap.put("scale_factor", OnnxTensor.createTensor(environment, FloatBuffer.wrap(scale), new long[]{1, 2}));

                // 运行推理
                try(OrtSession.Result output = session.run(onnxTensorHashMap)){
                    float[][] outputData = (float[][]) output.get(0).getValue();
                    for (float[] bbox : outputData) {
                        // bbox[类型索引,置信度,x1,y1,x2,y2]
                        if (bbox[1] < threshold) continue;
                        int clsId = (int)bbox[0];
                        Scalar scalar = new Scalar(odConfig.getOtherColor(clsId));
                        //绘制矩形区域
                        Imgproc.rectangle(img, new Point(bbox[2], bbox[3]), new Point(bbox[4], bbox[5]), scalar);
                        //绘制标签和置信度
                        drawText(img,bbox, LABELS[(int)bbox[0]] +" "+ bbox[1],scalar);
                    }
                }
                HighGui.imshow("Display Image", img);
                HighGui.waitKey();
            }
        }
        HighGui.destroyAllWindows();
        System.exit(0);
    }

    public static void drawText( Mat image ,float[] bbox,String text,Scalar backgroundColor){
        Point org = new Point(bbox[2], bbox[3]); // 文本左下角坐标
        int fontFace = Imgproc.FONT_HERSHEY_SIMPLEX;
        double fontScale = 0.5;
        int thickness = 1;
        int lineType = Imgproc.LINE_8;
        Scalar textColor = new Scalar(255, 255, 255); // 白色文本

        // 计算文本边界框大小
        Size textSize = Imgproc.getTextSize(text, fontFace, fontScale, thickness,null);
        int textWidth = (int) textSize.width;
        int textHeight = (int) textSize.height;

        // 绘制背景矩形
        Imgproc.rectangle(image,
                new Point(org.x , org.y + 5 ), // 左上角坐标（稍微扩展一些以确保文本完全覆盖）
                new Point(org.x + textWidth , org.y - textHeight ),
                backgroundColor,
                -1); // -1 表示填充矩形

        // 绘制文本
        Imgproc.putText(image, text, org, fontFace, fontScale, textColor, thickness, lineType);
    }

    public static OnnxTensor getRGBImage(OrtEnvironment environment, Mat img,Letterbox letterbox) throws OrtException {
        // 读取 image
        Mat image = img.clone();
        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2RGB);

        // 更改 image 尺寸
        image = letterbox.letterbox(image);

        int rows  = letterbox.getHeight();
        int cols  = letterbox.getWidth();
        int channels = image.channels();

        // 将Mat对象的像素值赋值给Float[]对象
        float[] pixels = new float[channels * rows * cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                double[] pixel = image.get(j,i);
                for (int k = 0; k < channels; k++) {
                    // 这样设置相当于同时做了image.transpose((2, 0, 1))操作
                    pixels[rows*cols*k+j*cols+i] = (float) pixel[k]/255.0f;
                }
            }
        }
        // 创建OnnxTensor对象
        long[] shape = { 1L, (long)channels, (long)rows, (long)cols };
        OnnxTensor tensor = OnnxTensor.createTensor(environment, FloatBuffer.wrap(pixels), shape);

        return tensor;
    }

    private static ODConfig odConfig = new ODConfig();
}
